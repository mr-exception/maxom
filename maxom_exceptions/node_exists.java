package maxom.maxom_exceptions;

public class node_exists extends Exception {
	String Id;

	public node_exists(String node_Id) {
		this.Id = node_Id;
	}

	@Override
	public String getMessage() {
		return ("node " + Id + " exists");
	}
}
