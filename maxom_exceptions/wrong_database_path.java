package maxom.maxom_exceptions;

public class wrong_database_path extends Exception {
	private String path;

	public wrong_database_path(String p) {
		this.path = p;
	}

	@Override
	public String getMessage() {
		return "wrong database directory: " + path + " , please use a empty folder or a saved database";
	}
}
