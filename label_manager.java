package maxom;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import maxom.maxom_exceptions.node_exists;

public class label_manager {
	private File root_dir;
	private node_manager nm;

	void set_node_manager(node_manager arg) {
		this.nm = arg;
	}

	public label_manager(File root) {
		this.root_dir = root;
	}

	relation create_relation(String start_node_Id, String end_node_Id) throws IOException {
		relation r = null;

		node nA = nm.get_node_by_Id(start_node_Id);
		node nB = nm.get_node_by_Id(end_node_Id);

		String relation_Id = start_node_Id + "@" + end_node_Id;
		
		File current_d = root_dir;
		boolean new_root_opened = false;
		for (int i = 0; i < relation_Id.length(); i++) {
			final String dir_name = String.valueOf(relation_Id.charAt(i));
			if (!new_root_opened) {
				File[] dirs = current_d.listFiles(new FileFilter() {
					@Override
					public boolean accept(File arg0) {
						if (!arg0.isDirectory())
							return false;
						return arg0.getName().equals(dir_name);
					}
				});
				if (dirs.length == 0) {
					current_d = new File(current_d.getPath() + STATICS.DIRECTORY_SPLITTER + dir_name);
					current_d.mkdir();
					new_root_opened = true;
				} else {
					current_d = dirs[0];
				}
			} else {
				current_d = new File(current_d.getPath() + STATICS.DIRECTORY_SPLITTER + dir_name);
				current_d.mkdir();
			}
		}
		if (new_root_opened)
			return new node(current_d);
		else {
			throw new node_exists(relation_Id);
		}
		return r;
	}
}
