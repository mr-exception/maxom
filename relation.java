package maxom;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class relation {
	private HashMap<String, String> property_map;
	private File relation_root;
	private File property_file;

	public relation(File dir) throws IOException {
		this.relation_root = dir;
		property_map = new HashMap<String, String>();

		if (!dir.exists() || !dir.isDirectory())
			dir.mkdir();
		property_file = new File(relation_root.getPath() + STATICS.DIRECTORY_SPLITTER + "property.dat");
		if (!property_file.exists())
			property_file.createNewFile();
	}

	public boolean add_property(String key, String value) throws IOException {
		Scanner in = new Scanner(new FileInputStream(property_file));
		boolean found_key = false;

		/* get file content to add it again */
		String file_content = "";
		if (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.startsWith(key)) {
				found_key = true;
				file_content = key + ":" + value;
			} else
				file_content = line;
			while (in.hasNextLine()) {
				line = in.nextLine();

				file_content += "\n";
				if (!found_key)
					if (line.startsWith(key)) {
						file_content += key + ":" + value;
						found_key = true;
					} else
						file_content += line;
				else
					file_content += line;
			}
			if (!found_key)
				file_content += "\n" + key + ":" + value;
		} else
			file_content = key + ":" + value;
		in.close();
		write_data_in_file(file_content, property_file);
		return true;
	}

	public String get_property(String key) throws FileNotFoundException {
		Scanner in = new Scanner(new FileInputStream(property_file));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.startsWith(key)) {
				String[] parts = line.split(":");
				return parts[1];
			}
		}
		return null;
	}

	private boolean write_data_in_file(String content, File f) throws IOException {
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(f));
		out.write(content.getBytes());
		out.close();
		return true;
	}

}
