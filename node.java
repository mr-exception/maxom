package maxom;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class node {
	private HashMap<String, String> property_map;
	private ArrayList<String> labels;
	private File node_root;

	private File property_file;
	private File label_file;

	public node(File dir) throws IOException {
		this.node_root = dir;
		property_map = new HashMap<String, String>();
		labels = new ArrayList<String>();

		if (!dir.exists() || !dir.isDirectory())
			dir.mkdir();
		property_file = new File(node_root.getPath() + STATICS.DIRECTORY_SPLITTER + "property.dat");
		label_file = new File(node_root.getPath() + STATICS.DIRECTORY_SPLITTER + "label.dat");
		if (!property_file.exists())
			property_file.createNewFile();
		if (!label_file.exists())
			label_file.createNewFile();
	}

	public boolean add_property(String key, String value) throws IOException {
		Scanner in = new Scanner(new FileInputStream(property_file));
		boolean found_key = false;

		/* get file content to add it again */
		String file_content = "";
		if (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.startsWith(key)) {
				found_key = true;
				file_content = key + ":" + value;
			} else
				file_content = line;
			while (in.hasNextLine()) {
				line = in.nextLine();

				file_content += "\n";
				if (!found_key)
					if (line.startsWith(key)) {
						file_content += key + ":" + value;
						found_key = true;
					} else
						file_content += line;
				else
					file_content += line;
			}
			if (!found_key)
				file_content += "\n" + key + ":" + value;
		} else
			file_content = key + ":" + value;
		in.close();
		write_data_in_file(file_content, property_file);
		return true;
	}

	public String get_property(String key) throws FileNotFoundException {
		Scanner in = new Scanner(new FileInputStream(property_file));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.startsWith(key)) {
				String[] parts = line.split(":");
				return parts[1];
			}
		}
		return null;
	}

	public boolean add_label(String label) throws IOException {
		Scanner in = new Scanner(new FileInputStream(label_file));
		String file_content = "";
		if (in.hasNextLine()) {
			file_content = in.nextLine();
			while (in.hasNextLine()) {
				String line = in.nextLine();

				file_content += "\n";
				if (line.equals(label))
					return true;
				file_content += line;
			}
			file_content += "\n" + label;
		} else
			file_content = label;
		write_data_in_file(file_content, label_file);
		return false;
	}

	public ArrayList<String> get_labels() throws FileNotFoundException {
		Scanner in = new Scanner(new FileInputStream(label_file));
		ArrayList<String> result = new ArrayList<String>();
		while (in.hasNextLine()) {
			String line = in.nextLine();
			result.add(line);
		}
		return result;
	}

	public boolean has_label(String label) throws FileNotFoundException {
		Scanner in = new Scanner(new FileInputStream(label_file));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.equals(label))
				return true;
		}
		return false;
	}

	private boolean write_data_in_file(String content, File f) throws IOException {
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(f));
		out.write(content.getBytes());
		out.close();
		return true;
	}

	public boolean remove() {
		if (node_root.exists() && node_root.isDirectory()) {
			File[] dir_list = node_root.listFiles(new FileFilter() {

				@Override
				public boolean accept(File arg0) {
					if (arg0.isDirectory())
						return true;
					else
						return false;
				}
			});
			File property_file = new File(node_root.getPath() + STATICS.DIRECTORY_SPLITTER + "property.dat");
			File label_file = new File(node_root.getPath() + STATICS.DIRECTORY_SPLITTER + "label.dat");
			property_file.delete();
			label_file.delete();

			if (dir_list.length > 0) {
				return true;
			} else {
				node_root.delete();
				return true;
			}
		} else {
			return false;
		}
	}
}
