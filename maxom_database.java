package maxom;

import java.io.File;
import java.io.IOException;

import maxom.maxom_exceptions.node_exists;
import maxom.maxom_exceptions.wrong_database_path;

public class maxom_database {
	private File db_path;
	private node_manager nm;

	public maxom_database(String database_path) throws wrong_database_path {
		this.db_path = new File(database_path);
		if (!db_path.exists()) {
			db_path.mkdir();
			System.out.println("new database created at " + database_path);
		} else {
			if (!db_path.isDirectory()) {
				throw new wrong_database_path(database_path);
			}
			System.out.println("connected to existing database");
		}
		nm = new node_manager(db_path);

		define_all_statics();
	}

	private void define_all_statics() {
		STATICS.DIRECTORY_SPLITTER = "\\";
	}

	/* node manager */
	public node create_node(String Id) throws IOException, node_exists {
		Id = Id.toLowerCase();
		return nm.create_node(Id);
	}

	public node get_node_by_Id(String Id) throws IOException {
		Id = Id.toLowerCase();
		return nm.get_node_by_Id(Id);
	}

	public boolean remove_node(String Id) {
		Id = Id.toLowerCase();
		return nm.remove_node(Id);
	}

	public static void main(String[] args) {
		try {
			maxom_database mdb = new maxom_database("db");
			node n = mdb.get_node_by_Id("TEST");
			n.remove();
		} catch (wrong_database_path e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
