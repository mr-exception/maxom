package maxom;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import maxom.maxom_exceptions.node_exists;

public class node_manager {
	private File root_dir;

	public node_manager(File root) {
		this.root_dir = root;
	}

	node create_node(String Id) throws IOException, node_exists {
		File current_d = root_dir;
		boolean new_root_opened = false;
		for (int i = 0; i < Id.length(); i++) {
			final String dir_name = String.valueOf(Id.charAt(i));
			if (!new_root_opened) {
				File[] dirs = current_d.listFiles(new FileFilter() {
					@Override
					public boolean accept(File arg0) {
						if (!arg0.isDirectory())
							return false;
						return arg0.getName().equals(dir_name);
					}
				});
				if (dirs.length == 0) {
					current_d = new File(current_d.getPath() + STATICS.DIRECTORY_SPLITTER + dir_name);
					current_d.mkdir();
					new_root_opened = true;
				} else {
					current_d = dirs[0];
				}
			} else {
				current_d = new File(current_d.getPath() + STATICS.DIRECTORY_SPLITTER + dir_name);
				current_d.mkdir();
			}
		}
		if (new_root_opened)
			return new node(current_d);
		else {
			throw new node_exists(Id);
		}
	}

	node get_node_by_Id(String Id) throws IOException {
		String path = root_dir.getPath();
		for (int i = 0; i < Id.length(); i++) {
			path += STATICS.DIRECTORY_SPLITTER + Id.charAt(i);
		}
		File result = new File(path);
		if (!result.exists()) {
			return null;
		} else {
			return new node(result);
		}
	}

	boolean remove_node(String Id) {
		String node_path = root_dir.getPath();
		for (int i = 0; i < Id.length(); i++)
			node_path += STATICS.DIRECTORY_SPLITTER + Id.charAt(i);
		File node_root = new File(node_path);
		if (node_root.exists() && node_root.isDirectory()) {
			File[] dir_list = node_root.listFiles(new FileFilter() {

				@Override
				public boolean accept(File arg0) {
					if (arg0.isDirectory())
						return true;
					else
						return false;
				}
			});
			File property_file = new File(node_path + STATICS.DIRECTORY_SPLITTER + "property.dat");
			File label_file = new File(node_path + STATICS.DIRECTORY_SPLITTER + "label.dat");
			property_file.delete();
			label_file.delete();

			if (dir_list.length > 0) {
				return true;
			} else {
				node_root.delete();
				return true;
			}
		} else {
			return false;
		}
	}
}
